# CAB302 Assignment 2

## Remote repository

Check the name of the remote repository:

	git remote -v
	
This should display something like:

	origin https://bitbucket.org/username/repo.git (fetch)
	origin https://bitbucket.org/username/repo.git (push)

## Get changes (Pull)

Assuming the name of the remote repo is "origin", to get any changes:

    git pull origin
  
## Save changes (Push)
  
Check if there are changed files:

    git status
  
To add new files or changes:
	
	git add .
	
Commit the changes to the repository history:

	git commit -m "What has changed message"
	
Push the changes on the "master" branch to the repository named "origin"

	git push origin master
	